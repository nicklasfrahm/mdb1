# Mechatronics Design and Build 1 (MDB1)

MDB1 is a project during the second semester of the MSc degree in mechatronics. This year the project is about using a robot arm to do conformal 3D printing of conductive filament.

## Development

You will need to have ROS Melodic installed and a catkin workspace with Python 3 set up. A tutorial of how to set up ROS can be found [here](http://wiki.ros.org/melodic/Installation/Ubuntu). Afterwards you can then create a catkin workspace with Python 3 as described [here](http://wiki.ros.org/catkin/Tutorials/create_a_workspace).

Make sure to also install the following packages via `apt`:

- `ros-melodic-joint-state-publisher-gui`
- `ros-melodic-trac-ik-kinematics-plugin`

Also ensure that the following packages are installed via `pip`:

- `rospkg`
- `catkin_pkg`

Clone the following repositories into the `src` folder of your catkin workspace:

- `https://github.com/CPR-Robots/cpr_robot`
- `https://gitlab.com/nicklasfrahm/mdb1`

Navigate to your catkin workspace and run the following commands to install this package and make it available:

```bash
$ cd $CATKIN_WORKSPACE
$ catkin clean
$ catkin config --extend /opt/ros/$ROS_DISTRO --cmake-args -DCMAKE_BUILD_TYPE=Release
$ catkin build
$ source devel/setup.bash
$ roslaunch mdb1 manual.launch
$ roslaunch mdb1 demo.launch
```

After this, your workspace should look similar to this:

```txt
├─ build/
|
├─ devel/
|  └─ setup.bash
|
├─ logs/
|
├─ src/
|  ├─ cpr_robot/
|  ├─ mdb1/
|  └─ CMakeLists.txt
|
└─ .catkin_workspace
```

### MoveIt

You can install MoveIt as described in [this tutorial](https://ros-planning.github.io/moveit_tutorials/doc/getting_started/getting_started.html).

## Utilities

To find the type of a ROS message use: `rostopic type $TOPIC`  
To list to the message on a ROS topic use: `rostopic echo $TOPIC`

## License

This project is licensed under the terms of the [MIT license](./LICENSE.md).
