#!/usr/bin/env python2
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import visualization_msgs.msg
import time
import math
import tf.transformations
import moveit_commander.conversions
import parser


def cartesian_controller():
    moveit_commander.roscpp_initialize(sys.argv)

    node_name = "cartesian_controller"
    rospy.init_node(node_name)

    # Instantiate a `RobotCommander`_ object. Provides information such as the robot's
    # kinematic model and the robot's current joint states
    robot = moveit_commander.RobotCommander()

    # Instantiate a `PlanningSceneInterface`_ object.  This provides a remote interface
    # for getting, setting, and updating the robot's internal understanding of the
    # surrounding world:
    scene = moveit_commander.PlanningSceneInterface()

    # Instantiate a `MoveGroupCommander`_ object.  This object is an interface
    # to a planning group (group of joints).  In this tutorial the group is the primary
    # arm joints in the Panda robot, so we set the group's name to "panda_arm".
    # If you are using a different robot, change this value to the name of your robot
    # arm planning group.
    # This interface can be used to plan and execute motions:
    group_name = "arm"
    move_group = moveit_commander.MoveGroupCommander(group_name)

    # Displaz Trajectory in RViz
    display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path',
                                                   moveit_msgs.msg.DisplayTrajectory,
                                                   queue_size=20)

    nozzle = moveit_commander.conversions.pose_to_list(
        move_group.get_current_pose().pose)[3:7]
    rospy.loginfo("nozzle (Quaternion):")
    rospy.loginfo(nozzle)

    origin = (0, 0, 0, 1)
    rospy.loginfo("origin (Quaternion):")
    rospy.loginfo(nozzle)

    transform = tf.transformations.quaternion_multiply(
        tf.transformations.quaternion_inverse(origin), nozzle)
    rospy.loginfo("transform (Quaternion")
    rospy.loginfo(transform)

    # waypoints = []

    # # IMPORTANT: The robot pose has to be deep copied, otherwise it will not move.
    # pose_a = copy.deepcopy(origin)
    # pose_a.position.x += 0.05
    # # pose_a.orientation.x = 0
    # # pose_a.orientation.y = -math.sqrt(2)/2
    # # pose_a.orientation.z = 0
    # # pose_a.orientation.w = math.sqrt(2)/2
    # # q_origin = moveit_commander.conversions.pose_to_list(pose_a)[3:7]
    # # q_rot = tf.transformations.quaternion_from_euler(-math.pi/2, 0, 0)
    # # q_new = tf.transformations.quaternion_multiply(q_origin, q_rot)
    # pose_a = orientation.vector_to_pose(pose_a, (0, 1, 0))
    # # rospy.loginfo("origin (euler):")
    # # rospy.loginfo(tf.transformations.euler_from_quaternion(q_origin))
    # # pose_a.orientation.x = q_new[0]
    # # pose_a.orientation.y = q_new[1]
    # # pose_a.orientation.z = q_new[2]
    # # pose_a.orientation.w = q_new[3]
    # rospy.loginfo("TARGET POSE:")
    # rospy.loginfo(pose_a)
    # waypoints.append(pose_a)

    # fobj = open("ad_lesbiam.txt")
    # for line in fobj:
    #     elements = line.strip().split(" ")
    #     elements = elements[1:len(elements)]

    #     for
    #     print line.strip()
    # fobj.close()

    # # pose_b = copy.deepcopy(pose_a)
    # # pose_b.position.z += 0.1
    # # waypoints.append(pose_b)
    # raw_input()
    # rospy.loginfo("goal (quaternion):")
    # rospy.loginfo(moveit_commander.conversions.pose_to_list(pose_a)[3:7])
    # rospy.loginfo(tf.transformations.euler_from_quaternion(
    #     moveit_commander.conversions.pose_to_list(pose_a)[3:7]))
    raw_input()
    waypoints = parser.parse_gcode(
        "/home/ubuntu/repos/uni/mdb1/src/mdb1/scripts/mdb1.gcode", (0.25, 0, 0.25))[0:50]
    waypoints.insert(0, copy.deepcopy(move_group.get_current_pose().pose))

    pub = rospy.Publisher(
        "/pose_publisher", geometry_msgs.msg.PoseStamped, queue_size=10, latch=True)
    pose_stamp = geometry_msgs.msg.PoseStamped()
    pose_stamp.header.frame_id = "base_link"
    pose_stamp.pose = waypoints[1]
    pub.publish(pose_stamp)

    # pub2 = rospy.Publisher(
    #     "/visualization_marker_array", visualization_msgs.msg.MarkerArray, queue_size=500, latch=True)

    # markers = visualization_msgs.msg.MarkerArray()
    # for waypoint in waypoints:
    #     marker = visualization_msgs.msg.Marker()
    #     marker.header.frame_id = "base_link"
    #     marker.type = marker.SPHERE
    #     marker.action = marker.ADD
    #     marker.scale.x = 0.0001
    #     marker.scale.y = 0.0001
    #     marker.scale.z = 0.0001
    #     marker.color.a = 1.0
    #     marker.color.r = 255
    #     marker.pose = waypoint
    #     markers.markers.append(marker)

    # identifier = 0
    # for m in markers.markers:
    #     m.id = identifier
    #     identifier += 1

    # pub2.publish(markers)

    # pose_a = copy.deepcopy(move_group.get_current_pose().pose)
    # pose_a.position.x += 0.1

    # waypoints.insert(1, pose_a)
    # for waypoint in waypoints:
    #     move_group.go(waypoint)

    move_group.set_goal_orientation_tolerance(1)
    move_group.set_num_planning_attempts(10)
    move_group.allow_replanning(True)
    rospy.loginfo(move_group.get_goal_orientation_tolerance())
    rospy.loginfo(move_group.get_goal_position_tolerance())

    # (plan, fraction) = move_group.compute_cartesian_path(
    #     waypoints, 0.00001, 0, avoid_collisions=False)
    # # rospy.loginfo(fraction)

    raw_input()
    move_group.go(waypoints[1])
    # move_group.execute(plan, wait=True)

    # Set frequency in Hz.
    frequency = 30
    rate = rospy.Rate(frequency)

    while not rospy.is_shutdown():
        # pose = move_group.get_current_pose().pose
        # pose.position.x -= 0.10

        # rospy.loginfo(pose)

        # move_group.set_pose_target(copy.deepcopy(pose))

        # plan = move_group.go(wait=True)

        # move_group.stop()
        # move_group.clear_pose_targets()

        # Delay next cycle to achieve desired frequency.
        rate.sleep()


if __name__ == "__main__":
    try:
        cartesian_controller()
    except rospy.ROSInterruptException:
        pass
