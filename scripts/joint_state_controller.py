#!/usr/bin/env python2
import rospy
import math
from sensor_msgs.msg import JointState


# Move joint linearly based on a defined trajectory.
def animate(waypoints, timestamp):
    # The period is the time of the last element of the waypoints.
    period = waypoints[-1]["time"]

    # Find the waypoint interval.
    interval = waypoints[1]["time"] - waypoints[0]["time"]

    # Find the current time getting remainder from the division of timestamp and period.
    time = timestamp % period

    # Find the current waypoint.
    index = int(math.floor(time / interval))

    # Calculate the current position.
    pos_current = waypoints[index]["position"]
    pos_next = waypoints[index + 1]["position"]
    percentage = (time - waypoints[index]["time"]) / interval
    step = pos_next - pos_current
    return pos_current + step * percentage


# Generate trajectory.
def generate_trajectory():
    span = math.pi / 8
    interval = 1
    positions = [0, span, 0, -span, 0]
    trajectory = []
    for i, position in enumerate(positions):
        trajectory.append({"time": i * interval, "position": position})
    return trajectory


# Generate joint names.
def generate_joint_state():
    joint_state = JointState()
    joint_state.name = [
        "base_link__link_01",
        "link_01__link_02",
        "link_02__link_03",
        "link_03__link_04",
        "link_04__link_05"
    ]
    return joint_state


# Simple linear controller for the robotic arm.
def controller():
    # Generate trajectory.
    trajectory = generate_trajectory()

    # Generate joint state.
    joint_state = generate_joint_state()

    # Configure publisher.
    topic = "/joint_states"
    pub = rospy.Publisher(topic, JointState, queue_size=10)

    # Use non-anonymous named node as only one controller may run at a time.
    node_name = "joint_state_controller"
    rospy.init_node(node_name)

    # Set frequency in Hz.
    frequency = 30
    rate = rospy.Rate(frequency)

    # Log simple message.
    rospy.loginfo("Starting simple linear trajectory controller ...")

    # Run control loop.
    while not rospy.is_shutdown():
        # Get time.
        timestamp = rospy.get_time()
        time = rospy.Time.from_sec(timestamp)

        # Get current position for given trajectory.
        pos = animate(trajectory, timestamp)

        # Set joint positions.
        joint_state.header.stamp = time
        joint_state.position = [pos, pos, pos, pos, pos]

        # Publish joint states.
        pub.publish(joint_state)

        # Delay next cycle to achieve desired frequency.
        rate.sleep()


if __name__ == "__main__":
    try:
        controller()
    except rospy.ROSInterruptException:
        pass
