#!/usr/bin/env python2

import math
import tf.transformations


def is_close(float1, float2, tolerance=1e-9):
    return abs(float1 - float2) <= tolerance


def vector_is_close(vec1, vec2):
    return is_close(vec1[0], vec2[0]) and is_close(vec1[1], vec2[1]) and is_close(vec1[2], vec2[2])


def dot_product(vec1, vec2):
    return vec1[0]*vec2[0] + vec1[1]*vec2[1] + vec1[2]*vec2[2]


def cross_product(vec1, vec2):
    return (
        vec1[1]*vec2[2] - vec1[2]*vec2[1],
        vec1[2]*vec2[0] - vec1[0]*vec2[2],
        vec1[0]*vec2[1] - vec1[1]*vec2[0]
    )


def vector_length(vec):
    return math.sqrt(vec[0]**2 + vec[1]**2 + vec[2]**2)


def invert_vector(vec):
    return (-vec[0], -vec[1], -vec[2])


def vector_to_axis_angle(vec):
    # Returns the axis angle orientation in the format (x, y, z, theta).
    # We assume origin to be a unit vector pointing in the negative y direction.
    vec_origin = (1, 0, 0)
    vec_origin_reversed = (-1, 0, 0)

    if vector_is_close(vec, vec_origin):
        return (0, 1, 0, 0)
    if vector_is_close(vec, vec_origin_reversed):
        return (0, 1, 0, math.pi)

    angle = math.acos(dot_product(vec_origin, vec))
    axis = cross_product(vec_origin, vec)
    length = vector_length(axis)

    return (axis[0]/length, axis[1]/length, axis[2]/length, angle)


def axis_angle_to_quaternion(axis_angle):
    # Returns the quaternion in the form of (x, y, z, w).
    rot = tf.transformations.quaternion_about_axis(
        math.pi/2, (1, 0, 0))  # Quaternion to rotate 90 degrees around x

    nozzle = tf.transformations.quaternion_multiply(
        (axis_angle[0]*math.sin(0.5*axis_angle[3]),
         axis_angle[1]*math.sin(0.5*axis_angle[3]),
         axis_angle[2]*math.sin(0.5*axis_angle[3]),
         math.cos(0.5*axis_angle[3])),
        (0.0, -math.sqrt(2)/2, 0.0,  math.sqrt(2)/2)
    )

    return tf.transformations.quaternion_multiply(nozzle, rot)
    # [-0.01644979 -0.70691404 -0.01638136  0.70691838]


def vector_to_pose(pose, vec):
    axis_angle = vector_to_axis_angle(vec)
    quaternion = axis_angle_to_quaternion(axis_angle)
    pose.orientation.x = quaternion[0]
    pose.orientation.y = quaternion[1]
    pose.orientation.z = quaternion[2]
    pose.orientation.w = quaternion[3]
    return pose

# test_cases = [
#     (1, 0, 0),
#     (0, 1, 0),
#     (0, 0, 1),
#     (math.sqrt(1.0/3.0), math.sqrt(1.0/3.0), math.sqrt(1.0/3.0))
# ]

# for test_case in test_cases:
#     axis_angle = vector_to_axis_angle(test_case)
#     quaternion = axis_angle_to_quaternion(axis_angle)
#     print "----------------------"
#     print axis_angle
#     print quaternion
