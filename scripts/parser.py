#!/usr/bin/env python2
import psutil
import os
import geometry_msgs.msg
import orientation


workspaceOffset = (0.25, 0, 0.25)


def waypoints_to_poses(waypoints, offset=(0, 0, 0)):
    # Calculate all poses.
    poses = []
    for waypoint in waypoints:
        pose = geometry_msgs.msg.Pose()
        pose.position.x = waypoint[0] + offset[0]
        pose.position.y = waypoint[1] + offset[1]
        pose.position.z = waypoint[2] + offset[2]

        # We invert the vector as the given vector is normal to the printed surface (pointed outward).
        direction_vector = orientation.invert_vector(waypoint[3:6])
        # Compute orientation quaternion.
        pose = orientation.vector_to_pose(pose, direction_vector)

        # Add next pose.
        poses.append(pose)

    return poses


def parse_gcode(filename, offset=(0, 0, 0)):
    # Open file.
    file = open(filename)

    # Store all waypoints.
    waypoints = []
    for line in file:
        elements = line.strip().split(" ")
        elements = elements[1:len(elements)]

        waypoint = []
        for element in elements:
            waypoint.append(float(element[1:len(element)]))

        waypoints.append(waypoint)

    return waypoints_to_poses(waypoints, offset)


if __name__ == "__main__":
    print "Parsing: scripts/mdb1.gcode"
    waypoints = parse_gcode("scripts/mdb1.gcode", workspaceOffset)
    print "Parsed %d waypoints!" % (len(waypoints))
    print "First pose:"
    print waypoints[0]
