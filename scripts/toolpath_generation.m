%------------------------------------------------------------------------%
%           Tool Path generation for MDB1: (Stereo Jack 6.35mm)          %
%------------------------------------------------------------------------%

% It generates three matrices "x","y" and "z" and each layer is generated
% by each row of these three matrices. e.g: Row 1 of x,y and z make the 
% first layer (1-19) of conductive filament and last layer of non-conductive
% on jack.

clc;
clear all;

t = 0:pi/20:100*pi;     %Samples
n=length(t);            %no of samples
m=20;                   %No of layers

x = zeros(m,n);         %Creating Matrix of (m*n) for x
y = zeros(m,n);         %Creating Matrix of (m*n) for y
z = zeros(m,n);         %Creating Matrix of (m*n) for z

r = 6.3/2;              %Initial Radius of jack
h = 0;                  %Height of filament
rev = 0;                %Variable for reversing direction


%------------------------------------------------------------------------%
%               Loop for making circle of different radius.              %
%------------------------------------------------------------------------%

% create 20 circles with different radius, increasing by 0.2 mm in radius
% --> generate 20 flat circles

for k=1:m     
    for i=1:n 
       % circle complete when x == 0
       x(k,i) = r*sin(t(i));
       % crcle complete when y == 1
       y(k,i) = r*cos(t(i));
    end
    % increase the radius by 0.2mm after each circle completion
    r=r+0.2;
end


%------------------------------------------------------------------------%
%               Loop for increment values of height                      %
%------------------------------------------------------------------------%

temp1 = 41; %Temporary variable (A circle completes after every 41 samples)
temp2 = 1;  %Temporary variable (To Skip values for reversing circle)
r = 1;      %Radius of circle

for k=1:m       % for all layers ( 20 layers)   
    for i=1:n   % for all circles (Nr of samples)
        
        % rev is counter
        % n = 41 --> 1 circle complete
        if rev > 1 && mod(rev,2)== 0    
            x(k,i) = x(k,temp1);
            %temp1= temp1 - 1;
            if temp2 == 1   %Skip value for the first time in "X"
                temp1 = temp1 - 2;
            else
              temp1 = temp1 - 1;
            end
            temp2 = 0;
        else
            %If the value is not reversing than just simply dublicate the
            %value.
            if temp2 == 0
                x(k,i) = x(k,i-1);
            end
            temp2 = 1;
        end
        
        % increments the values in z-direction3
        % circle complete when value == 1
        % k = number of layers
        % i = number of samples
        if y(k,i) ~= r*1            % while circle i not complete
            z(k,i) = z(k,i) + h;    % set z-coordinate equal to height
        % while z-coordinate is below/equal 16mm increase
        elseif z(k,i) <= 16
            z(k,i) = z(k,i) + h;
            % when z-coordinate = 8mm or 12mm, make a bigger gap by
            % increasing h by 1.2mm 
            if (int8(z(k,i)) == 8 || int8(z(k,i)) == 12)  && (k ~= 20)
                h= h+1.2;
                temp1 = 41;
                rev = rev + 1; 
            % increase vertically normally (0.4mm) otherwise
            else        
                h= h+0.4;
                temp1 = 41;
                rev = rev + 1;                 
            end
        end
        
       % when we reach the final height, don't make any changes in our coordinates 
       if int8(z(k,i)) == 16
            for j = i:n
                x(k,j) = x(k,i);
                y(k,j) = y(k,i);
                z(k,j) = z(k,i);
            end
            break
            
       end
    end
    
    % updates for the next outer layer
    temp2 = 1;      % reset temp2
    rev = 0;        % reset rev
    h=0;            % reset h 
    r=r+0.2;        % increase the radius by 0.2mm
end


%------------------------------------------------------------------------%
%                          Writing in CSV file                           %
%------------------------------------------------------------------------%

% create a column_vector where all the points will be stored
x_column = zeros(m*n,1);
y_column = zeros(m*n,1);
z_column = zeros(m*n,1);

% limit of actual useful points per helix
lim = 1359;
% for the 20th helix we have more values
lim_20 = 1520;          % +161 values since we don't have air gaps
delta_20 = 161;         %  store it in a variable, too

% for troubleshooting: 
% create two containers with the row-number of the start & end point. 
% These rows are equal to the span of one transposed row
% i.e the span covers 2001 cells
container_row_begin = zeros(20,1);
container_row_end = zeros(20,1);

% fot the first 19 layers
for i = 1:m
    % collect all the values of the current row 
    % from value 2 up to value 1360 
    if i <= 19
        row_xi_transposed = transpose(x(i,2:lim+1));
        row_yi_transposed = transpose(y(i,2:lim+1));
        row_zi_transposed = transpose(z(i,2:lim+1));
        
        % calculate start and ending position 
        row_start = (i-1)*lim+1;
        row_end = row_start+lim-1;
    
    elseif i == 20
        row_xi_transposed = transpose(x(i,2:lim_20+1));
        row_yi_transposed = transpose(y(i,2:lim_20+1));
        row_zi_transposed = transpose(z(i,2:lim_20+1));     
        
        % calculate start and ending position 
        row_start = (i-1)*lim+1;
        row_end = row_start+lim_20-1;
    end 
    
    % put the obtained values in the column vectors 
    x_column(row_start:row_end,1) = row_xi_transposed;
    y_column(row_start:row_end,1) = row_yi_transposed;
    z_column(row_start:row_end,1) = row_zi_transposed;
        
    % update the container with the start & end points for troubleshooting
    container_row_begin(i,1) = row_start;
    container_row_end(i,1) = row_end;
end

% show the start and end points of the row numbers as a table
start_end_points = [container_row_begin, container_row_end];

% set up the matrix which contains all the necessary values 
x_y_z_matrix = [x_column(1:m*lim+delta_20), y_column(1:m*lim+delta_20), z_column(1:m*lim+delta_20)];


%------------------------------------------------------------------------%
%                          Normal Vectors                                %
%------------------------------------------------------------------------%
% Reference link:
% https://math.stackexchange.com/questions/2570089/normal-to-a-cylinder-in-cartesian-coordinates

unit_normalize_vec = zeros(m*lim,3);
norm_x = zeros(m*lim+delta_20,1);
norm_y = zeros(m*lim+delta_20,1);
norm_z = zeros(m*lim+delta_20,1);


% create unit vectors in x & y direction, z.coord. = 0 
for i=1:m*lim+delta_20
    
    % IMPORTANT: we divide by "norm(x_y_z_matrix(i,1:2)"
    % i.e divide only by the norm of x & y coordinate 
    norm_x = x_y_z_matrix(i,1)./norm(x_y_z_matrix(i,1:2));
    norm_y = x_y_z_matrix(i,2)./norm(x_y_z_matrix(i,1:2));
    %norm_z = x_y_z_matrix(i,3)./norm(x_y_z_matrix(i,:));
    
    unit_normalize_vec(i,1)=norm_x;
    unit_normalize_vec(i,2)=norm_y;
    %unit_normalize_vec(i,3)=norm_z;
end

% for troubleshooting: 
% check the length of all unit vectors
unit_length = zeros(lim,1);
for i=1:m*lim
    unit_length(i) = sqrt(unit_normalize_vec(i,1)^2 + unit_normalize_vec(i,2)^2);
end

% convert units from mm to m:
x_y_z_matrix = x_y_z_matrix./1000;

% combine all the points in a matrix and save the final result
% matrix columns: x,y,z,i,j,k
combine = [x_y_z_matrix,unit_normalize_vec];  
writematrix(combine,'Combine.csv');         % save result as csv file


%------------------------------------------------------------------------%
%                          Plotting Normal Vectors                       %
%------------------------------------------------------------------------%

% % Let's make it easier to understand and rename start and end points
helix_start = container_row_begin;
helix_end = container_row_end;
% 
% % a = [2 3 5]; % your point [x0,y0,z0]
% % b = [1 1 0]; % your normal vector 
% % quiver3(a(1), a(2), a(3), b(1), b(2), b(3));
% 
% % plot the normal vectors of the inner most helix
% % (the number in helix_start() and helix_end() defines the helix)
% % (--> 1 = innermost helix, 20 = outermost helix)
%  for i = helix_start(1):helix_end(1)
%     quiver3(x_y_z_matrix(i,1),x_y_z_matrix(i,2),x_y_z_matrix(i,3),unit_normalize_vec(i,1),unit_normalize_vec(i,2),unit_normalize_vec(i,3));
%     hold on;
%  end

 
%------------------------------------------------------------------------%
%                          Plotting Layers                               %
%------------------------------------------------------------------------%

% create 20 cells. Each cell contains all the data for one helix
helix = cell(m,1);
 
for i=1:m
    % cut out the correct cells values of the x_y_z_matrix and store the  
    % data in the current helix
    helix_x = x_y_z_matrix(helix_start(i):helix_end(i),1);
    helix_y = x_y_z_matrix(helix_start(i):helix_end(i),2);
    helix_z = x_y_z_matrix(helix_start(i):helix_end(i),3);
    helix{i} = [helix_x,helix_y,helix_z];
end

% Plot first helix:
plot3(helix{1}(:,1),helix{1}(:,2),helix{1}(:,3));
hold on

% % Plot the last helix
% plot3(helix{20}(:,1),helix{20}(:,2),helix{20}(:,3));

% % Plot all 20 helixes:
% for i=1:m
%     plot3(helix{i}(:,1),helix{i}(:,2),helix{i}(:,3));
%     hold on
% end

% label axis for display
xlabel('x');
ylabel('y');
zlabel('z');

% Generate GCode file.
file = fopen("mdb1.gcode", "w+");
coordinates = ["X", "Y", "Z", "I", "J", "K"];
[rows, cols] = size(combine);
for i = 1:rows
    fprintf(file, "G1");
    for j = 1:cols
        fprintf(file, " %s%.8f", coordinates(j), combine(i, j));
    end
    fprintf(file, "\n");
end
fclose(file);
